# Chilean Seismic Catalogue 1982 - mid-2020

This repository contains the relocated catalogue of the _Centro Sismológico Nacional_ (CSN, Universidad de Chile, http://www.sismologia.cl).

Contact: 
- Bertrand Potin, _DGF, University of Chile_ (bertrand.potin@uchile.cl)
- Sergio Ruiz, _DGF, University of Chile_ (sruiz@uchile.cl)

![seismicity_map_alone](./wd/seismicity_map_alone.jpg){width=60%}

_Chilean seismicity as reported by the CSN. **Left**: relocated CSN catalogue, from 1982 to mid-2020. Circled points represent large earthquakes (magnitude $\ge$ 7.5) that occured since 1900, as reported by the ISC (International Seismological Center, http://www.isc.ac.uk/). Shallower earthquakes are plotted on top. **Right**: Clusters identifyed in this study._

Catalogues for both the seismicity and the clusters are formatted into CSV archives.

## How to cite this material

### Material doi

https://doi.org/10.5281/zenodo.13146436

### Related article

Potin, B., S. Ruiz, F. Aden-Antoniow, R. Madariaga, and S. Barrientos (2024). A Revised Chilean Seismic Catalog from 1982 to Mid-2020, _Seismol. Res. Lett._. https://doi.org/10.1785/0220240047

## Files format

### CHILE_SEISMICITY_RELOCATED.csv

The file contains 1 header line and 118004 event lines (1 line per event) corresponding to all events between 1982 and mid-2020 relocated for this study.

#### Columns description:
- **#**: number of the event from 0 to 118003, in chronological order,
- **year**: event origin time year (YYYY),
- **month**: event origin time month (MM),
- **day**: event origin time day (DD),
- **hour**: event origin time hours (hh, [0,23]),
- **minute**: event origin time minutes (mm, [0,59]),
- **second**: event origin time seconds (ss.ss, [0.00,59.99]),
- **longitude**: hypocentre longitude (xxxx.xxxx, [-76.2294,-64.8110]),
- **latitude**: hypocentre latitude (xxx.xxxx, [-45.9796,-17.8178]),
- **depth**: hypocentre geographical depth (xxx.xxxx, [-4.9540,336.5503]),
- **RMS**: Root-mean-square of data adjustment in location process (see below for details),
- **magnitude**: magnitude value, either a number [0.20,8.80] or empty when no magnitude was determined,
- **magnitude_type**:
    - ```l```: local magnitude,
    - ```c```: coda magnitude,
    - ```w```: moment magnitude determined following Brune's spectral approach,
    - ```ww```: moment magnitude determined following the W-phase approach,
    - ```xx```: no magnitude.

#### RMS computation:

The RMS is computed as follow:
$$\mathrm{RMS} = \sqrt{\frac{1}{n}\sum_{i=1}^n(t_i^{obs}-t_i^{calc})^2}$$ 
where $n$ is the number of arrival times of either P- or S-waves for the event, $t^{obs}$ is the observed arrival-time and $t^{calc}$ is the computed arrival-time determined as $t^{calc} = t_0 + T(\mathbf{x})$ with $t_0$ the origin time and $T(\mathbf{x})$ the propagation time between the hypocentre $\mathbf{x}$ and the station. 

#### file format example:
```
...
100870,2018,1,26,6,57,12.06,-71.403518,-29.438705,57.5149,0.26,2.8,l 
100871,2018,1,26,9,50,6.16,-67.257799,-23.912037,190.2663,0.32,3.0,l 
100872,2018,1,26,12,54,38.73,-71.32775,-31.009157,50.2409,0.31,4.1,l 
100873,2018,1,26,13,27,5.3,-74.226795,-37.488853,21.4607,0.3,5.1,ww
100874,2018,1,26,15,4,52.48,-72.550192,-29.705678,16.8856,0.43,3.2,l 
100875,2018,1,26,15,26,23.56,-73.808079,-37.575196,18.9708,0.6,5.0,ww
100876,2018,1,26,15,50,17.33,-68.686646,-21.865756,117.7657,0.27,3.2,l
...
```

---

### CHILE_CLUSTERS_RELOCATED.csv

The file cointains 1 header line and 29263 event lines (1 line per event).

#### Columns description:
- **year**: event origin time year (YYYY),
- **month**: event origin time month (MM),
- **day**: event origin time day (DD),
- **hour**: event origin time hours (hh, [0,23]),
- **minute**: event origin time minutes (mm, [0,59]),
- **second**: event origin time seconds (ss.ss, [0.00, 59.99]),
- **longitude**: hypocentre longitude (xxxx.xxxx, [-76.2294,-64.8110]),
- **latitude**: hypocentre latitude (xxx.xxxx, [-45.9796,-17.8179]),
- **depth**: hypocentre geographical depth (xxx.xxxx, [-4.9540, 336.5503]),
- **RMS**: Root-mean-square of data adjustment in location process (see above for details),
- **magnitude**: magnitude value, either a number [2.43, 8.80] or empty when no magnitude was determined,
- **magnitude_type**:
    - ```l```: local magnitude,
    - ```c```: coda magnitude,
    - ```w```: moment magnitude determined following Brune's spectral approach,
    - ```ww```: moment magnitude determined following the W-phase approach,
    - ```xx```: no magnitude.
- **label**: number between 0 and 48 used to identify clusters. Label order is random and do not indicate clasification of clusters. Some numbers are missing from the list because they correspond to deep clusters that where not included in the article.

#### file format example:
```
...
2017,3,8,13,33,55.53,-33.862827,-71.447657,58.5711,0.35,3.6,l,23
2017,7,16,8,19,31.55,-33.914015,-71.28985,49.62,0.19,4.0,l,23
2017,7,19,8,6,19.82,-33.871165,-71.348819,58.1362,0.23,3.7,l,23
2017,9,23,22,2,2.37,-33.761952,-71.504656,49.2679,0.43,4.9,ww,23
2017,10,27,2,8,18.04,-33.89703,-71.421792,50.3903,0.55,2.6,l,23
...
```
